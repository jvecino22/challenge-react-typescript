import '../styles/globals.scss'
import 'bootstrap/dist/css/bootstrap.min.css'

// @ts-ignore
function MyApp({ Component, pageProps }) {
    return <Component {...pageProps} />
}

export default MyApp
