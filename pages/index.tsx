import Layout from '../components/Layout'
import List from "../components/List";

const IndexPage = () => (
    <Layout>
        <List/>
    </Layout>
)

export default IndexPage
