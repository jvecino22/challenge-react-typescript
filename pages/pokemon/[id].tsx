import { GetServerSideProps } from 'next'
import Layout from '../../components/Layout'
import ListDetail from '../../components/ListDetail'
import {getPokemons, getPokemonsType} from "../../service/pokemons";

type Props = {
    item?: any
    type?: any
    errors?: string
}

const StaticPropsDetail = ({ item, type, errors }: Props) => {
    if (errors) {
        return (
            <Layout title="Error to load data">
                <p>
                    <span style={{ color: 'red' }}>Error:</span> {errors}
                </p>
            </Layout>
        )
    }

    return (
        <Layout
            title={`Pokemon | ${
                item ? item.name : 'Pokemon Detail'
            } | Detail`}
        >
            {item && <ListDetail item={item} type={type} />}
        </Layout>
    )
}

export default StaticPropsDetail

// This function gets called at build time on server-side.
export const getServerSideProps: GetServerSideProps = async ({ params }) => {
    try {
        const id = params?.id
        const item = await getPokemons(id)
        const type = await getPokemonsType(item?.types[0].type.name)
        // By returning { props: item }, the StaticPropsDetail component
        // will receive `item` as a prop at build time
        return { props: { item, type } }
    } catch (err) {
        return { props: { errors: err.message } }
    }
}
