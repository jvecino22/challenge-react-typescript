import * as fetch from 'node-fetch';

export function getPokemons(id) {
    return fetch(`https://pokeapi.co/api/v2/pokemon/${id}`).then(res => res.json())
}

export function getPokemonsType(type) {
    return fetch(`https://pokeapi.co/api/v2/type/${type}`).then(res => res.json())
}
