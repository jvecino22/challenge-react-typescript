import React from 'react'
import Link from 'next/link'

type Props = {
  data: any
}

const ListItem = ({ data }: Props) => (
  <Link href="/pokemon/[id]" as={`/pokemon/${data.id}`}>
    <a className={'mya'}>
        <div className={'cards'}>
            <div className={'cards__picture'}><img src={data.sprites.front_default} width={'115'} height={'115'} /></div>
            <div className={'cards__details'}>
                <ul className={'mya'}>
                    <li className={'mya'}>
                        <small>Pokedex Number </small><strong>#{data.id}</strong>
                    </li>
                    <li>
                        {data.name}
                    </li>
                </ul>
                {data.types.map((d: any) => (
                    <div className={'cards__details--type-pokemon '}><p className={d.type.name}>{d.type.name}</p></div>
                ))}
            </div>

        </div>
    </a>
  </Link>
)

export default ListItem
