import * as React from 'react'
import ListItem from './ListItem'
import {useEffect, useState} from "react";
import { getPokemons } from "../service/pokemons"

const List = () => {
  const [pokemons, setPokemons] = useState([])

  useEffect(() => {
     if (pokemons.length < 500) {
      getPokemons(pokemons.length + 1).then(async (d: any) => {
        const item = await d
        // @ts-ignore
        setPokemons([ ...pokemons, item])
      })
    }
  }, [pokemons])

return (
  <div className={'d-flex flex-wrap justify-content-center'}>
    {pokemons.map((item: { id: any; name?: string }) => (
        <ListItem data={item} />
    ))}
  </div>
)}

export default List
