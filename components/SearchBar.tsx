import * as React from 'react'
import { useRouter } from 'next/router'
import { useRef } from "react";

const SearchBar = () => {
    const inputSearch = useRef<HTMLInputElement>(null)
    const router = useRouter()
    const getSearch = (e: any) => {
        e.preventDefault()
        const search = inputSearch.current!.value
        router.push(`/pokemon/${search}`)
    }
    return(
        <form>
            <div className={'container'}>
               <div className={'row align-items-center'}>
                  <div className={'col-3 col-md-2'}><img src={'/pokemon.svg'} height={'70'} width={'160'} /></div>
                  <div className={'col-6 col-md-8'}><input className={'control-form'} type={'text'} ref={inputSearch} alt={'Search'} placeholder={'FInd you pokemo by its Pokedex Number'}/></div>
                  <div className={'col-3 col-md-2'}><button className={'botton'} onClick={getSearch}>Find</button></div>

               </div>
            </div>
        </form>
)}

export default SearchBar
