import React, { ReactNode } from 'react'
import Head from 'next/head'
import SearchBar from "./SearchBar";

type Props = {
  children?: ReactNode
  title?: string
}

const Layout = ({ children, title = 'Pokemon | Search' }: Props) => {
    return (
  <div>
    <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
        <link rel="manifest" href="/site.webmanifest" />
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
        <meta name="msapplication-TileColor" content="#da532c" />
        <meta name="theme-color" content="#ffffff" />
    </Head>
    <header>
        <SearchBar />
    </header>
    <hr/>
    {children}
    <footer>
      <hr />
      <span>Julio Vecino (c) 2021</span>
    </footer>
  </div>
)}

export default Layout
