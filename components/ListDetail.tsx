import * as React from 'react'

type ListDetailProps = {
  item: any
  type: any
}

const ListDetail = ({ item: pokemon, type: pokemonType }: ListDetailProps) => (
  <div className={'container'}>
      <div className={"row"}>
          <div className={"col-12 col-md-3"}>
              <div className={"images"}>
                  <div className={"images__pokemon images__default"}>
                      <p className={"images__type normal-pok"}>Normal</p>
                      <img src={pokemon.sprites.front_default} className="images__img" alt="Front" />
                      <img src={pokemon.sprites.back_default} className="images__img" alt="Back" />
                  </div>
                  <div className={"images__pokemon images__shiny"}>
                      <p className="images__type shiny">Shiny</p>
                      <img src={pokemon.sprites.front_shiny} className="images__img" alt="Front" />
                      <img src={pokemon.sprites.back_shiny} className="images__img" alt="Bank" />
                  </div>
              </div>
          </div>
          <div className="col-6 col-md-2">
              <div className="form-infos">
                  <label>Pokedex</label>
                  <div className="form-infos__input">{pokemon.id}</div>
                  <label>Height</label>
                  <div className="form-infos__input">{pokemon.height}</div>
              </div>
              <div className="types">
                  <label>Types</label>
                  <ul className={"types__list-types"}>
                      { pokemon.types.map((t: any) => (
                          <li className={t.type.name} key={`type-${t.type.name}`}>{t.type.name}</li>
                      ))}
                  </ul>
              </div>
          </div>
          <div className="col-6 col-md-3">
              <div className="form-infos">
                  <label>Name</label>
                  <div className="form-infos__input capitalize">{pokemon.name}</div>
                  <label>Weight</label>
                  <div className="form-infos__input">{pokemon.weight}</div>
                  <div className="abilities">
                      <label>Abilities</label>
                      <ul className="abilities__list-abilities">
                          {pokemon.abilities.map((a: any) => (
                              <li key={`ability-${a.ability.name}`}>{a.ability.name}</li>
                          ))}
                      </ul>
                  </div>
              </div>
          </div>
          <div className="col-12 col-md-4">
              <div className="types-damage">
                  <label>Double damage to</label>
                  <ul className="types-damage__list-types">
                      {pokemonType.damage_relations.double_damage_to.map((rf: any) => (
                          <li className={rf.name} key={`ddto-${rf.name}`}>{rf.name}</li>
                      ))}
                  </ul>
              </div>
              <div className="types-damage">
                  <label>Double damage from</label>
                  <ul className="types-damage__list-types">
                      {pokemonType.damage_relations.double_damage_from.map((rf: any) => (
                          <li className={rf.name} key={`ddfrom-${rf.name}`}>{rf.name}</li>
                      ))}
                  </ul>
              </div>
              <div className="types-damage">
                  <label>Half damage from</label>
                  <ul className="types-damage__list-types">
                      {pokemonType.damage_relations.half_damage_from.map((rf: any) => (
                          <li className={rf.name} key={`hdfrom-${rf.name}`}>{rf.name}</li>
                      ))}
                  </ul>
              </div>
              <div className="types-damage">
                  <label>Half damage to</label>
                  <ul className="types-damage__list-types">
                      {pokemonType.damage_relations.half_damage_to.map((rf: any) => (
                          <li className={rf.name} key={`hdto-${rf.name}`}>{rf.name}</li>
                      ))}
                  </ul>
              </div>
          </div>
      </div>
      <div className="row">
          <div className="col-12 col-md-3">
              <div className="videogames">
                  <label><strong>Games where appears</strong></label>
                  <ul className="videogames__list">
                      {pokemon.game_indices.map((g: any) => (
                          <li  key={`video-${g.version.name}`}>{g.version.name}</li>
                      ))}
                  </ul>
              </div>
          </div>
          <div className="col-12 col-md-9">
              <div className="moves">
                  <label><strong>Moves</strong></label>
                  <ul className="moves__list">
                      {pokemon.moves.map((m: any) => (
                          <li  key={`moves-${m.move.name}`}>{m.move.name}</li>
                      ))}
                  </ul>
              </div>
          </div>
      </div>
 </div>
)

export default ListDetail
