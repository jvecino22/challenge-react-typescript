# Pokedex Challenge

Create an interface where the user can see all the pokemon and their details.
## Requirements:
For this application, we need two different screens. The first one to show a list with all pokemon,
the second one to show the details of a specific pokemon.

- Principal/main screen:
    - Header:
        - Add the pokemon logo at the left, it should take the user to the Principal
          Screen when clicking on it.
        - In the middle add a text field to find a specific pokemon, user can find it by
          name and Pokedex number.
        - Add a button at the right to search the pokemon, this button sends the user
          to the Detailsscreen.

- Details screen:
    - Header:
        - Keep the same header as Principal Screen.
    - Body/main container:
        - Show the different pictures of the pokemon (normal and shiny).
        - Show the following pokemon information:
    - Pokedex Number.
    - Name.
    - Height.
    - Weight.
    - Type(s).      
 
## Installation of dependencies

```
yarn install
```
## Run the aplication

```
npm run dev
```

## Route server estarted
```
http://localhost:3000
```
